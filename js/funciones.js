const guardarContacto = (db, contacto)=>{

     if(!contacto.nombre||!contacto.numero||!contacto.direccion){
      
        let nombre = document.getElementById('nombre');
        let numero = document.getElementById('numero');
        let direccion = document.getElementById('direccion');
        let formulario = document.getElementById('formulario');
       
        nombre.classList.add('input-incorrecto');
        numero.classList.add('input-incorrecto');
        direccion.classList.add('input-incorrecto');

        let alerta = document.getElementById("alerta");
        alerta.style.display='flex';

        addEventListener("blur",()=>{

        })



        nombre.addEventListener("blur",()=>{
            if(nombre.value.length > 0){
                nombre.classList.remove('input-incorrecto');
                nombre.classList.add('input-correcto');
                console.log("cosas")
                alerta.style.display = 'none';
            }else{
                nombre.classList.remove('input-correcto');
                nombre.classList.add('input-incorrecto');
                alerta.style.display='flex';
            }
        })
        numero.addEventListener("blur",()=>{
            if(numero.value.length > 0){

                numero.classList.remove('input-incorrecto');
                numero.classList.add('input-correcto');
                console.log("cosas")
                alerta.style.display = 'none';
            }else{
                numero.classList.remove('input-correcto');
                numero.classList.add('input-incorrecto');
                alerta.style.display='flex';
            }
        })
        direccion.addEventListener("blur",()=>{
            if(direccion.value.length > 0){
                direccion.classList.remove('input-incorrecto');
                direccion.classList.add('input-correcto');
                console.log("cosas")
                alerta.style.display = 'none';
            }else{
                direccion.classList.remove('input-correcto');
                direccion.classList.add('input-incorrecto');
                alerta.style.display='flex';
            }
        }) 
    
     }else{

        db.setItem(contacto.id, JSON.stringify(contacto));
        window.location.href = '/'; 
     }
    
  
}

const CrearMensaje = ()=>{
    
    let listadoVacio = document.getElementById('id-listado');
    let divVacio = document.createElement('div');
    let mensaje = document.createElement('h3');
    mensaje.innerHTML= "La lista contactos esta vacia";
    mensaje.classList.add('mensajeVacio')
    divVacio.appendChild(mensaje);
    listadoVacio.appendChild(divVacio)
    
}

const cargarContactos = (db,parentNode) =>{
    let claves = Object.keys(db);

    if(!db.getItem(claves)){
        CrearMensaje()
    }

    for(clave of claves){
        let contacto = JSON.parse(db.getItem(clave))
        crearContacto(parentNode, contacto,db);  
    }
}

const crearContacto = (parentNode, contacto, db) =>{

    let divContacto = document.createElement('div');
    let nombreContacto = document.createElement('h3');
    let numeroContacto = document.createElement('p');
    let direccionContacto = document.createElement('p');
    let borrarIcon = document.createElement('span');

    nombreContacto.innerHTML = contacto.nombre;
    numeroContacto.innerHTML = contacto.numero;
    direccionContacto.innerHTML = contacto.direccion;
    borrarIcon.innerHTML = 'delete_forever'

    divContacto.classList.add('tarea');
    borrarIcon.classList.add('material-icons','icon')

    borrarIcon.onclick = () =>{
        db.removeItem(contacto.id)
        window.location.href = '/'
    }

    divContacto.appendChild(nombreContacto);
    divContacto.appendChild(numeroContacto);
    divContacto.appendChild(direccionContacto);
    divContacto.appendChild(borrarIcon);

    parentNode.appendChild(divContacto);
    

}